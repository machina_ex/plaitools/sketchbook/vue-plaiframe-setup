# Install

`yarn install`

# Run

`yarn run dev`

# Configuration

Set configurations in \*./public/**config.json\***

```JSON
  "API_URL": "https://adaptordev.machinaex.org/api",
```

Set to your API endpoint.

```JSON
  "LOG_STORE": false,
```

Set _true_ to print every store action and mutation.

```JSON
   "LOG_LEVEL_APP": "debug"
```

Not yet implemented. Will control wich toasts are displayed.

# VS Code and Vite Setup (with ESLint and Prettier)

<https://vueschool.io/articles/vuejs-tutorials/eslint-and-prettier-with-vite-and-vue-js-3/>

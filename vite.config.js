import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import eslintPlugin from "vite-plugin-eslint";
import { resolve } from "path";

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: [
      { find: "@", replacement: resolve(__dirname, "./src") },
      {
        find: "@components",
        replacement: resolve(__dirname, "./src/components"),
      },
      {
        find: "@utils",
        replacement: resolve(__dirname, "./src/utils"),
      },
    ],
  },
  plugins: [vue(), eslintPlugin({ cache: false })],
  server: { port: 3000 },
});

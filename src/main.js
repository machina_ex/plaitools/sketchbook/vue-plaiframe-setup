import { createApp } from "vue";
import createRouter from "@/router";
import createStore from "@/store";
import { Manager } from "socket.io-client";
import Toast, { POSITION } from "vue-toastification";
import Api from "@utils/rest";
import App from "@/App.vue";
import Icon from "@components/Template/Icon.vue";
import "@/styles/main.scss";
import { hasKey } from "@utils";

async function main() {
  try {
    /*
     * 1. Get config file from public folder and parse it.
     */
    const response = await fetch("/config.json");
    if (response.status !== 200) throw new Error(`Could not load config file from ${response.url}`);
    const config = await response.json();

    if (!config.API_URL) {
      config.API_URL = `${window.location.origin}/api`;
    }

    /*
     * 2. Init rest api client and socket manager.
     */
    const client = new Api({ baseURL: config.API_URL });
    const manager = new Manager(config.API_URL, {
      reconnectionDelayMax: Infinity,
      autoConnect: false,
    });
    // console.log("Socket Manager", manager);

    /*
     * 3. Create vue app, store and router. Pass manager to socket store plugin.
     */
    const app = createApp(App);
    const router = createRouter();
    const store = createStore(config, manager);

    /*
     * 4. Register api event callbacks. See API constructor for other events.
     */
    const errorToast = (e) => {
      const { name, message } = hasKey(e, "response.data.message")
        ? e.response.data
        : { name: "response error", message: `${e.toString()}.\n${e.request.responseURL}` };
      const text = `${name.toUpperCase()}\n${message}`;
      store.dispatch("adaptor/addLog", { type: "error", message: text });
    };
    client.on("successIssue", ({ name, message }) => {
      const text = `${name.toUpperCase()}\n${message}`;
      store.dispatch("adaptor/addLog", { type: "warning", message: text });
    });
    client.on("successPrompt", ({ message, webhook, schema, method }) => {
      store.dispatch("adaptor/triggerPrompt", { message, webhook, schema, method });
    });
    client.on("responseError", errorToast);
    client.on("noResponseError", errorToast);
    client.on("authError", () => store.dispatch("adaptor/setAuth", false));

    /*
     * 5. Link api client in store. Inside store it can be accesses by this.$api.
     */
    store.$api = client;
    store.$router = router;

    /*
     * 6. Register store action on every route change.
     */
    router.beforeEach((to) => store.dispatch("adaptor/initView", { view: to.name, ...to.params, query: to.query }));

    // FIXME/TODO/NOTE: We run initView after nextTick to not trigger watchers before component unmount. But some components need the initView before mount, so we leave it for now. Figure this out
    // router.afterEach((to) =>
    //   nextTick().then(() => store.dispatch("adaptor/initView", { view: to.name, ...to.params }))
    // );

    /*
     * 7. Reset values from local storage and check if we need to show login overlay on app start.
     */
    await store.dispatch("adaptor/restore", {});

    /*
     * 8. Add plugins and component and mount app.
     */
    app.component("Icon", Icon).use(Toast, { position: POSITION.BOTTOM_RIGHT }).use(router).use(store).mount("#app");
  } catch (error) {
    console.error(error);
    const errorDiv = document.createElement("div");
    const errorMsg = document.createTextNode(error.toString());
    errorDiv.appendChild(errorMsg);
    document.body.prepend(errorDiv);
  }
}

main();

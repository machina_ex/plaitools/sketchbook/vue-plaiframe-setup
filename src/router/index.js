import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "GamesList",
    component: () => import("@/views/GamesList.vue"),
  },
  {
    path: "/game/:game",
    name: "Overview",
    component: () => import("@/views/GameOverview.vue"),
  },
  {
    path: "/game/:game/settings",
    name: "Settings",
    component: () => import("@/views/GameSettings.vue"),
  },
  {
    path: "/game/:game/dashboard",
    name: "Dashboard",
    component: () => import("@/views/GameDashboard.vue"),
  },
  {
    path: "/game/:game/collections",
    name: "Collections",
    component: () => import("@/views/GameCollections.vue"),
  },
  {
    path: "/game/:game/editor/:level",
    name: "Editor",
    component: () => import("@/views/LevelEditor.vue"),
  },
  {
    path: "/game/:game/config/:level",
    name: "Config",
    component: () => import("@/views/LevelConfig.vue"),
  },
  {
    path: "/404",
    name: "NotFound",
    component: () => import("@/views/NotFound.vue"),
  },
  { path: "/:catchAll(.*)", redirect: "/404" },
];

export default () => {
  const router = createRouter({
    history: createWebHistory(),
    routes,
  });
  return router;
};

import { merge } from "lodash";
import { i18n, isUrlSafe, hasKeyPresentAndPopulated } from "@utils";

const state = {
  names: [],
  schema: {}, // list of all collections and their schemas
  documents: {}, // list of all collections and their documents
};

const getters = {};

const actions = {
  clear({ commit }) {
    commit("CLEAR_COLLECTION_DATA");
  },
  getAll({ rootState, commit }, game) {
    if (!game) game = rootState.game.selected;
    this.$api
      .getCollections({ game })
      .then((r) => {
        commit("SET_COLLECTIONS_DATA", { key: "names", value: Object.keys(r.data) });
        commit("SET_COLLECTIONS_DATA", { key: "schema", value: r.data });
      })
      .catch(console.error);
  },
  getDocuments({ rootState, state, commit, dispatch }, { game, collection }) {
    if (!game) game = rootState.game.selected;
    if (!hasKeyPresentAndPopulated(state.schema, `${collection}.properties`)) {
      dispatch("getAll", game);
    }
    this.$api
      .findDocuments({ game, collection })
      .then((r) => {
        commit("SET_COLLECTIONS_DOCUMENTS", { key: collection, value: r.data });
      })
      .catch(console.error);
  },
  createDocument({ rootState, dispatch }, { game, collection, document }) {
    if (!game) game = rootState.game.selected;
    if (typeof document === "string") {
      try {
        document = JSON.parse(document);
      } catch (error) {
        const message = i18n("ERROR.JSONCONVERT", { error: error.toString() });
        dispatch("adaptor/addLog", { type: "error", message }, { root: true });
        return Promise.reject(error);
      }
    }
    this.$api
      .createDocument({ game, collection }, document)
      .then(() => dispatch("getDocuments", { game, collection }))
      .catch(console.error);
  },
  updateDocument({ rootState, dispatch }, { game, collection, id, data }) {
    if (!game) game = rootState.game.selected;
    return this.$api
      .updateDocument({ game, collection, document: id }, data)
      .then(() => dispatch("getDocuments", { game, collection }))
      .catch(console.error);
  },
  editDocument({ rootState }, { game, collection, id, data }) {
    if (!game) game = rootState.game.selected;
    return (
      this.$api
        .editDocument({ game, collection, document: id, operator: "_set" }, data)
        // .then(commit("setItem", { id, key, value }))
        .catch(console.error)
    );
  },
  deleteDocument({ rootState, dispatch }, { game, collection, document }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .deleteDocument({ game, collection, document })
      .then(() => dispatch("getDocuments", { game, collection }))
      .catch(console.error);
  },
  createCollection({ rootState, state, dispatch }, { game, name }) {
    if (!game) game = rootState.game.selected;
    if (state.names.includes(name)) {
      const message = i18n("ERROR.DUPLICATE", { target: "collection", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("duplicate collection name: " + name));
    } else if (!isUrlSafe(name) || name === undefined) {
      const message = i18n("ERROR.CHARACTERS", { target: "collection", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("forbbiden characters in name: " + name));
    } else {
      this.$api
        .createCollection({ game }, { name })
        .then(() => dispatch("getAll", game))
        .catch(console.error);
    }
  },
  deleteCollection({ rootState, dispatch }, { game, collection }) {
    if (!game) game = rootState.game.selected;
    this.$api
      .deleteCollection({ game, collection })
      .then(() => dispatch("getAll", game))
      .catch(console.error);
  },
};

const mutations = {
  CLEAR_COLLECTION_DATA(state) {
    state.schema = {};
    state.data = {};
  },
  SET_COLLECTIONS_DATA(state, { key, value }) {
    state[key] = value;
  },
  UPDATE_COLLECTIONS_DATA(state, { key, value }) {
    merge(state[key], value);
  },
  SET_COLLECTIONS_DOCUMENTS(state, { key, value }) {
    state.documents[key] = value;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

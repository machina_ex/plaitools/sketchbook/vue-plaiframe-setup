import { cloneDeep, merge } from "lodash";
import { isUrlSafe, i18n } from "@utils";

const state = {
  selected: "", // name of selected game
  actions: [],
  collections: [], // list of string names of collections in game
  levelList: [],
  levelSchema: {},
  media: [],
  setup: {},
};

const getters = {
  levelNames(state) {
    return state.levelList.map((level) => level.name);
  },
  // Note: backend should provide these infos; see issue: #396
  extendedLevelList: (state, getters, rootState) => {
    const levels = cloneDeep(state.levelList);
    if (!levels) return [];
    if (!rootState.plugins.plugins) return levels;
    else {
      rootState.plugins.plugins.forEach((plugin) => {
        if (!plugin.items) return;
        Object.entries(plugin.items).forEach(([collection, data]) => {
          if (!data.items) return;
          data.items.forEach((item) => {
            if (!item.settings.level) return;
            const level = levels.find((e) => e.name === item.settings.level);
            if (level)
              level.default = {
                level: item.settings.level,
                item: item.name,
                plugin: plugin.name,
                collection: collection,
              };
          });
        });
      });
      return levels;
    }
  },
  actionsByPlugin: (state) => {
    const all = state.actions;
    return all?.reduce((result, currentValue) => {
      if (!result[currentValue["plugin"]]) result[currentValue["plugin"]] = [];
      result[currentValue["plugin"]].push(currentValue);
      return result;
    }, {});
  },
  assetForToolbar: (state) => (key) => {
    let collection = state[key].map((item) => {
      let newItem = {};
      let arr = item.split(".");
      let depth = arr.length - 1;
      let title = arr[depth].replace("]]", "").replace("[[", "");
      newItem["value"] = item;
      newItem["title"] = title;
      newItem["depth"] = depth;
      return newItem;
    });
    return collection;
  },
};

const actions = {
  clear({ state, commit }, game) {
    if (game !== state.selected) commit("CLEAR_GAME_DATA");
  },
  select({ dispatch, commit }, game) {
    commit("SELECT_GAME", game);
    dispatch("getGame", game);
  },
  async getGame({ commit, dispatch }, game) {
    this.$api
      .getGame({ game })
      .then((result) => {
        const { actions, collections, level, plugins, setup } = result.data;
        commit("SET_GAME_DATA", { key: "actions", value: actions });
        commit("SET_GAME_DATA", { key: "collections", value: collections });
        commit("SET_GAME_DATA", { key: "levelList", value: level });
        dispatch("plugins/updateInstalled", plugins, { root: true });
        commit("SET_GAME_DATA", { key: "setup", value: setup });
      })
      .catch((error) => {
        // TODO: reroute to game selection and error message
        console.error(error);
      });
  },
  updateLevelList({ rootState, state, commit, dispatch }, levelList) {
    commit("SET_GAME_DATA", { key: "levelList", value: levelList });

    const currView = rootState.adaptor.view;
    if (currView == "Editor" || currView == "Config") {
      const updatedLevelEntry = levelList.find((l) => l._id === rootState.level.id);

      // check if currently opened level has been deleted
      if (!updatedLevelEntry) {
        const message = i18n("ERROR.DELETE", {});
        dispatch("adaptor/addLog", { type: "error", message }, { root: true }).then(() =>
          this.$router.push({ name: "Overview", params: { game: state.selected } }),
        );
        return;
      }
      // check if name of currently opened level has been changed
      if (updatedLevelEntry.name !== rootState.level.selected) {
        dispatch("level/unselectAll", null, { root: true }).then(() => {
          const message = i18n("INFO.CHANGE", {});
          dispatch("adaptor/addLog", { type: "info", message }, { root: true });
          this.$router.replace({ params: { game: state.selected, level: updatedLevelEntry.name } });
        });
        return;
      }
    }
  },
  updateActionsList({ commit }, actions) {
    // TODO/NOTE: not tested because backend never sent message
    commit("SET_GAME_DATA", { key: "actions", value: actions });
  },
  updateMediaList({ commit }, media) {
    // TODO/NOTE: not tested because no file upload to test if backend sends message
    commit("SET_GAME_DATA", { key: "media", value: media });
  },
  createLevel({ state, getters, dispatch }, { level }) {
    const game = state.selected;
    const name = level.name;
    if (getters.levelNames.includes(name)) {
      const message = i18n("ERROR.DUPLICATE", { target: "level", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("duplicate game name: " + name));
    } else if (!isUrlSafe(name) || name === undefined) {
      const message = i18n("ERROR.CHARACTERS", { target: "level", name });
      dispatch("adaptor/addLog", { type: "error", message }, { root: true });
      return Promise.reject(new Error("forbbiden characters in name: " + name));
    } else {
      this.$api
        .createLevel({ game }, level)
        .then(() => dispatch("getGame", state.selected))
        .catch(console.error);
    }
  },
  deleteLevel({ state, dispatch }, { game, level }) {
    this.$api
      .deleteLevel({ game, level })
      .then(() => dispatch("getGame", state.selected))
      .catch(console.error);
  },
};

const mutations = {
  SELECT_GAME(state, game) {
    state.selected = game;
  },
  CLEAR_GAME_DATA(state) {
    state.selected = "";
    state.actions = [];
    state.collections = [];
    state.levelList = [];
    state.levelSchema = [];
    state.plugins = [];
    state.setup = {};
  },
  SET_GAME_DATA(state, { key, value }) {
    state[key] = value;
  },
  UPDATE_GAME_DATA(state, { key, value }) {
    merge(state[key], value);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

const state = {
  selected: [],
};

const getters = {};

const actions = {
  select({ state, dispatch }) {
    dispatch("setIsLive", !state.isLive);
  },
};

const mutations = {
  SET_SELECTION(state, open) {
    state.isLive = open;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

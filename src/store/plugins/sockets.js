import Socket from "@utils/socket";

const createSocketsPlugin = (manager, debugLog = false) => {
  return (store) => {
    // prettier-ignore
    const LOG = new Socket(manager, store, (level) => `/log/${level}`,
      {
        log: (data, store) => store.dispatch("serverLog/addMessages", data),
      }, debugLog
    );

    // prettier-ignore
    const GAME = new Socket(manager, store, (game) => `/game/${game}`,
      {
        level: (data) => store.dispatch("game/updateLevelList", data),
        plugin: (data) => store.dispatch("plugins/updateInstalled", data),
        actions: (data) => store.dispatch("game/updateActionsList", data),
        media: (data) => store.dispatch("game/updateMediaList", data),
      }, debugLog
    );

    // prettier-ignore
    const SESSIONS = new Socket(manager, store, (game) => `/game/${game}/live/sessions`,
      {
        changed: (data, store) => store.dispatch("live/updateSessions", data),
        removed: (data, store) =>  store.dispatch("live/removeSessions", data),
      }, debugLog
    );

    // prettier-ignore
    const ITEMS = new Socket(manager, store, (game) => `/game/${game}/live/items`,
      {
        changed: (data, store) => {store.dispatch("live/updateItems", data); store.dispatch("plugins/updateItems", data)},
        removed: (data, store) => {store.dispatch("live/removeItems", data); store.dispatch("plugins/removeItems", data)}
      }, debugLog
    );

    // LEVEL can also send messages: enter_state, leave_state, user_interaction
    // prettier-ignore
    const LEVEL = new Socket(manager, store, (game, level) => `/game/${game}/level/${level}`,
       {
        user_joined: (data) => store.dispatch("adaptor/addOtherUser", data),
        user_left: (data) => store.dispatch("adaptor/removeOtherUser", data),
        state_update: (data) => store.dispatch("level/updateStates", data),
        state_deleted: (data) => store.dispatch("level/deleteStateLocal", data.id),
        state_busy: (data) => store.dispatch("level/blockState", data),
        state_clear: (data) => store.dispatch("level/releaseState", data),
        variables: (data) => store.dispatch("level/updateVariables", data),
        path_update: (data) => store.dispatch("level/updatePaths", data),
        user_interaction: (data) => console.log("user_interaction",data),
       }, debugLog
     );

    store.subscribe((mutation, state) => {
      switch (mutation.type) {
        case "serverLog/TOGGLE":
        case "serverLog/CHANGE_LOG_LEVEL":
          if (state.serverLog.isOpen) LOG.open(state.serverLog.level);
          else LOG.close();
          break;
        case "game/SELECT_GAME":
          GAME.open(state.game.selected);
          break;
        case "game/CLEAR_GAME":
          GAME.close();
          break;
        case "level/SELECT_LEVEL_ID":
          if (state.level.id) LEVEL.open(state.game.selected, state.level.id);
          else LEVEL.close();
          break;
        case "level/SELECT_STATE":
          if (state.level.selectedState) LEVEL.emit("enter_state", state.level.selectedState);
          else LEVEL.emit("leave_state");
          break;
        case "level/CLEAR_LEVEL":
          LEVEL.close();
          break;
        case "live/SET_IS_LIVE":
          if (state.live.isLive) {
            ITEMS.open(state.game.selected);
            SESSIONS.open(state.game.selected);
          } else {
            ITEMS.close();
            SESSIONS.close();
          }
          break;
        case "adaptor/CHANGE_VIEW":
          // console.log(state.adaptor.view);
          if (state.adaptor.view === "Settings") ITEMS.open(state.game.selected);
          else ITEMS.close();
          break;
      }
    });
  };
};

export default createSocketsPlugin;
